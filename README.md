# JEHC-MEDICAL开源平台，单体工程版本。

### 前言

打造一流的世界级医学病历云影像平台。

本平台暂时实现了电子病历，影像学，实验室检验，患者信息，检验套餐，患者绑定病历，处方，就诊记录，医院信息维护，科室管理，医院级别，药品管理，药商管理，科室分类，科室管理，患者注册，上传影像学报告，实验室检查项配置，授权中心，系统平台，日志平台，附件平台，OMP，WEB端监控等。

集成AI完成对影像资料的精准识别 不放过任何 病情的诊断。

联系方式：244831954@qq.com / hxtkdcj@163.com / 540539727@qq.com / 微信：13611572774

 **访问地址** 

star一下呢？


影像学系统地址：

http://47.116.169.203

用户名/密码

ZYS/xskj123123

DYS/xskj123123

LYS/xskj123123


 **技术栈** 

 **后端** 

Spring，
SpringBoot2.0，
Mybatis，
PageHelper，
Solr全文检索，
Redis，
Ehcache，
JWT，
Oauth2，
数据库读写分离，
Activity5.22工作流，
客户端负载均衡Rule，
Sentinel限流体系，
Nacos注册中心 配置中心，
Gateway网关，
Junit，
Netty，
Quartz调度器，
FTP，
ES全文检索,
Openoffice，
Onvif摄像头,
OpenCV,
Mqtt,
ffmpeg



 **前端** 

可视化流程设计器，
VUE，
Bootstrap4+，
ElementUI,
perfect-scrollbar,
fontawesome,
jstree,
Jquery2，
DataTables，
Mxgraph，
PDFJS，
ZTree,
SVGEditor,
VTK,
ITK,
video等

 **开发工具** 

  eclipse-jee-mars-1、eclipse-jee-mars-2、eclipse-juno、STS、IDEA

#### 安装教程

1. 安装mysql5.7++数据库（其它数据库如Oracle）
2. 安装IntelliJ IDEA 2017.3.2 x64开发工具
3. 安装apache-maven-3.2.1及本地库repository
4. 安装Redis3版本以上
5. 导入jehc-medical工程项目
6. 设置maven环境

#### 工程结构

| ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E5%B7%A5%E7%A8%8B/%E5%90%8E%E7%AB%AF.jpg)  |![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E5%B7%A5%E7%A8%8B/%E5%89%8D%E7%AB%AF.jpg)|
|---|---|



 **授权中心数据结构**
![输入图片说明](https://images.gitee.com/uploads/images/2021/0313/221812_6cc2e51a_1341290.png "JEHC-CLOUD微服务授权中心数据结构.png") 


#### 功能模块

 **医学平台**
| ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E7%97%85%E5%8E%86%E4%BA%91%E7%9B%B8%E5%85%B3/%E7%97%85%E5%8E%86%E7%AE%A1%E7%90%86.jpg) |  ![输入图片说明](screenshot/HIS/N2.jpg) |
|---|---|
| ![输入图片说明](screenshot/HIS/1.0.2/6.jpg) | ![输入图片说明](screenshot/HIS/1.0.2/2.jpg) |
| ![输入图片说明](screenshot/HIS/1.0.2/3.jpg)  | ![输入图片说明](screenshot/HIS/1.0.2/4.jpg)  |
| ![输入图片说明](screenshot/HIS/1.0.2/5.jpg)  | ![输入图片说明](screenshot/HIS/1.0.2/6.jpg)  |
|  ![输入图片说明](screenshot/HIS/N7.jpg) |  ![输入图片说明](screenshot/HIS/N8.jpg) |
|  ![输入图片说明](screenshot/HIS/N9.jpg) |  ![输入图片说明](screenshot/HIS/N10.jpg) |
|  ![输入图片说明](screenshot/HIS/N11.jpg) |  ![输入图片说明](screenshot/HIS/N12.jpg) |
|  ![输入图片说明](screenshot/HIS/N13.jpg) |  ![输入图片说明](screenshot/HIS/N14.jpg) |
|  ![输入图片说明](screenshot/HIS/N15.jpg) |  ![输入图片说明](screenshot/HIS/N16.jpg) |
|  ![输入图片说明](screenshot/HIS/N17.jpg) |  ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E7%97%85%E5%8E%86%E4%BA%91%E7%9B%B8%E5%85%B3/%E6%82%A3%E8%80%85%E4%BF%A1%E6%81%AF.jpg) |
|  ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E7%97%85%E5%8E%86%E4%BA%91%E7%9B%B8%E5%85%B3/%E5%A4%84%E6%96%B9.jpg)|  ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E7%97%85%E5%8E%86%E4%BA%91%E7%9B%B8%E5%85%B3/%E7%97%85%E5%8E%86%E7%BB%91%E5%AE%9A%E8%B4%A6%E6%88%B7.jpg) |
|  ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E7%97%85%E5%8E%86%E4%BA%91%E7%9B%B8%E5%85%B3/%E6%96%B0%E5%BB%BA%E6%82%A3%E8%80%85%E4%BF%A1%E6%81%AF%E5%B9%B6%E6%B3%A8%E5%86%8C.jpg) |  ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E7%97%85%E5%8E%86%E4%BA%91%E7%9B%B8%E5%85%B3/%E5%AE%9E%E9%AA%8C%E5%AE%A4%E6%A3%80%E6%9F%A5%E7%BB%84%E5%A5%97%E9%A4%90%E9%85%8D%E7%BD%AE.jpg) |
| ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E7%97%85%E5%8E%86%E4%BA%91%E7%9B%B8%E5%85%B3/%E5%8C%BB%E9%99%A2%E6%A3%80%E6%9F%A5%E9%A1%B9%E7%9B%AE.jpg)  |  ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E7%97%85%E5%8E%86%E4%BA%91%E7%9B%B8%E5%85%B3/%E5%8C%BB%E9%99%A2%E6%A3%80%E6%9F%A5%E9%A1%B9%E7%9B%AE%E8%AE%BE%E5%A4%87.jpg) |
| ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/%E7%97%85%E5%8E%86%E4%BA%91%E7%9B%B8%E5%85%B3/%E5%B0%B1%E8%AF%8A%E8%AE%B0%E5%BD%95.png) |   |

**授权中心**
| ![输入图片说明](%E6%96%87%E6%A1%A3/%E7%97%85%E5%8E%86%E4%BA%91%E5%B9%B3%E5%8F%B0/login.png)  |  ![输入图片说明](screenshot/%E8%A7%92%E8%89%B2%E6%9D%83%E9%99%90.jpg) |
|---|---|
| ![输入图片说明](screenshot/%E8%B5%84%E6%BA%90%E8%8F%9C%E5%8D%95.jpg)  | ![输入图片说明](screenshot/%E6%A8%A1%E5%9D%97%E5%8D%B8%E8%BD%BD.jpg)  |
| ![输入图片说明](screenshot/Index.jpg) | ![输入图片说明](screenshot/%E8%B4%A6%E5%8F%B7%E7%AE%A1%E7%90%86.jpg)  |



**系统**
| ![输入图片说明](screenshot/SYS/N1.jpg)  |  ![输入图片说明](screenshot/SYS/N2.jpg) |
|---|---|
|  ![输入图片说明](screenshot/SYS/N3.jpg) |  ![输入图片说明](screenshot/SYS/N4.jpg) |
|  ![输入图片说明](screenshot/SYS/N5.jpg) |  ![输入图片说明](screenshot/SYS/N6.jpg) |
|  ![输入图片说明](screenshot/SYS/N7.jpg) |  ![输入图片说明](screenshot/SYS/N8.jpg) |
|  ![输入图片说明](screenshot/SYS/N9.jpg) |  ![输入图片说明](screenshot/SYS/N10.jpg) |
|  ![输入图片说明](screenshot/SYS/N11.jpg) |  ![输入图片说明](screenshot/SYS/N12.jpg) |
|  ![输入图片说明](screenshot/SYS/N13.jpg) |  ![输入图片说明](screenshot/SYS/N14.jpg) |
|  ![输入图片说明](screenshot/SYS/N15.jpg) |  ![输入图片说明](screenshot/SYS/N16.jpg) |
|  ![输入图片说明](screenshot/SYS/N17.jpg) |  ![输入图片说明](screenshot/SYS/N18.jpg) |
|  ![输入图片说明](screenshot/SYS/N19.jpg) |  ![输入图片说明](screenshot/SYS/N20.jpg) |
|  ![输入图片说明](screenshot/SYS/N21.jpg) |   |


**运管**
| ![输入图片说明](screenshot/OMP/N1.jpg)  |  ![输入图片说明](screenshot/OMP/N2.jpg) |
|---|---|
|  ![输入图片说明](screenshot/OMP/N3.jpg) |  ![输入图片说明](screenshot/OMP/N4.jpg) |
|  ![输入图片说明](screenshot/OMP/N5.jpg) |  ![输入图片说明](screenshot/OMP/N6.jpg) |
|  ![输入图片说明](screenshot/OMP/N7.jpg) |  ![输入图片说明](screenshot/OMP/N8.jpg) |
|  ![输入图片说明](screenshot/OMP/N9.jpg) |  ![输入图片说明](screenshot/OMP/N10.jpg) |
|  ![输入图片说明](screenshot/OMP/N11.jpg) |   |


**调度**

![输入图片说明](screenshot/%E8%B0%83%E5%BA%A6%E5%99%A8.jpg)

**文档**
![输入图片说明](screenshot/%E6%96%87%E6%A1%A3%E5%9C%A8%E7%BA%BF%E9%A2%84%E8%A7%88.jpg)
![输入图片说明](screenshot/%E9%99%84%E4%BB%B6.jpg)


**日志**

![输入图片说明](screenshot/%E6%97%A5%E5%BF%97%E4%B8%AD%E5%BF%83.jpg)

**我们基于JEHC-2.0开源平台开发。该项目一直在维护中。** 

### 微服务版本

集成产品线版本：https://gitee.com/jehc/jehc-cloud.git

工作流版本：https://gitee.com/jehc/jehc-cloud-workflow

IOT版本：https://gitee.com/jehc/jehc-cloud-iot

报表版本：https://gitee.com/jehc/jehc-cloud-report

病历云版本：https://gitee.com/jehc/jehc-cloud-medical

### 单工程2.0版本
纯净版本：https://gitee.com/jehc/jehc

工作流版本：https://gitee.com/jehc/jehc-workflow

IOT版本：https://gitee.com/jehc/jehc-iot

病历云（HIS）版本：https://gitee.com/jehc/jehc-medical

### 单工程2.0前端版本
1.传统工程jehc-front（挂靠2.0.0分支，新分支已废弃）

2.VUE版本jehc-front-vue（新增）

### 单工程1.0版本
纯净版本：https://gitee.com/jehc/jehc-boot